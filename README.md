# SL Minesweeper

Third project for the "Scripting languages and their applications" course at 
Gdansk University of Technology.

The goal of this project was to describe the planned functionalites of 
an application and then create two versions of it using PyGTK and PyQt. 
Both versions of the application must fulfil all functionalities specified 
earlier. The selected application was minesweeper.

Can you guess which one of the two versions was giving me more trouble? 🤡

## Music
"Getting it Done" Kevin MacLeod (incompetech.com)<br/>
Licensed under Creative Commons: By Attribution 4.0 License<br/>
http://creativecommons.org/licenses/by/4.0/

## Sound effects
All of the sound effects are from zapsplat.com