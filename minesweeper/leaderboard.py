import json
import pathlib
from datetime import datetime


SCORES_PER_BOARD = 10


class Score:
	def __init__(self, name, game_time, date):
		self._name = name
		if isinstance(date, str):
			self._date = datetime.strptime(date, '%d/%m/%y %H:%M:%S')
		else:
			self._date = date
		self._game_time = game_time

	def get_name(self):
		return self._name

	def get_game_time(self):
		return self._game_time

	def get_date(self):
		return self._date

	def __gt__(self, other):
		return self._date > other.get_date() \
			if self._game_time == other.get_game_time \
			else self._game_time < other.get_game_time()

	def __lt__(self, other):
		return self._date < other.get_date() \
			if self._game_time == other.get_game_time \
			else self._game_time > other.get_game_time()

	def get_dict(self):
		date_str = self._date.strftime('%d/%m/%y %H:%M:%S')
		return {'name': self._name, 'game_time': self._game_time, 'date': date_str}


class LeaderBoard:
	def __init__(self, path, board_names):
		self.path = path
		self.board_names = board_names

		try:
			file_path = pathlib.Path(path)
			if file_path.exists():
				with open(file_path, 'r') as f:
					self.scores_json = json.load(f)
					for bname in board_names:
						if bname not in self.scores_json:
							self.scores_json[bname] = []
			else:
				with open(file_path, 'w') as f:
					f.write('{}')
					self.scores_json = {bname: [] for bname in self.board_names}
		except IOError:
			self.scores_json = {bname: [] for bname in self.board_names}
			print(f'Failed to access file {path}!')

		self.scores = {
			bname: [Score(**score) for score in self.scores_json[bname]]
			for bname in self.board_names
		}

	def check_score(self, bname, score):
		score = Score("", score, datetime.now())
		return len(self.scores[bname]) < SCORES_PER_BOARD or self.scores[bname][-1] > score

	def add_score(self, bname, score, uname):
		score = Score(uname, score, datetime.now().replace(microsecond=0))
		self.scores[bname].append(score)
		self.scores[bname] = [*reversed(sorted(self.scores[bname]))][:10]

	def write_scores(self):
		scores_json = {}
		for bname in self.board_names:
			score_dict_list = [score.get_dict() for score in self.scores[bname]]
			scores_json[bname] = score_dict_list
		with open(self.path, 'w') as f:
			json.dump(scores_json, f)

	def get_scores(self, bname):
		return self.scores[bname]


if __name__ == "__main__":
	leader_board = LeaderBoard('../leaderboard.json', ['łatwy', 'średni', 'trudny'])
	score = Score('nobody', 999, datetime.MINYEAR)
	leader_board.add_score('łatwy', score)
	print(leader_board.get_scores('łatwy'))
	leader_board.write_scores()
