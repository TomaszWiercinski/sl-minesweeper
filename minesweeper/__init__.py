from .game_logic import *
from .leaderboard import *

if __name__ == "__main__":
	import os

	def cls():
		os.system('cls' if os.name == 'nt' else 'clear')

	game = Game(width=9, height=9, mine_count=80)
	leaderboard_easy = LeaderBoard(
		path='../leaderboard.json',
		board_names=['easy', 'medium', 'hard']
	)
	leaderboard_easy.check_score('easy', 10)
	leaderboard_easy.write_scores()
	timer = Timer()
	game_state = 0

	timer.start()
	while game_state == 0:
		print(game, '\n')
		x = int(input('X'))
		y = int(input('Y'))
		# cls()
		if game.uncover(x, y):
			game_state = -1
		elif game.check_win_condition():
			game_state = 1
	timer.stop()

	if game_state == -1:
		print('GAME OVER!')
		print(f'Time: {timer.get_time():0.2}s')