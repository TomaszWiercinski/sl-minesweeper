import itertools
import random
import logging


logging.basicConfig(level=logging.DEBUG)
DIFF_EASY = {"width": 9, "height": 9, "mine_count": 10}
DIFF_MEDIUM = {"width": 16, "height": 16, "mine_count": 40}
DIFF_HARD = {"width": 32, "height": 16, "mine_count": 99}
MIN_WIDTH = 3
MIN_HEIGHT = 3
MAX_WIDTH = 32
MAX_HEIGHT = 32


class Game:
	def __init__(self, width, height, mine_count):
		"""Game logic controller.

		:param width: width of the game map
		:param height: height of the game map
		:param mine_count: number of mines, cannot be lower than one or greater than `width * height - 1`
		"""

		# Check game parameters - map size and mine count.
		width = max(min(width, MAX_WIDTH), MIN_WIDTH)
		height = max(min(height, MAX_HEIGHT), MIN_HEIGHT)
		mine_count = max(min(mine_count, width*height-1), 1)

		self.width = width
		self.height = height
		self.mine_count = mine_count
		self._uncovered = 0
		self.fun_win = None
		self.fun_loss = None
		self.mines_initialized = False
		self._initialize_map()

	def __str__(self):
		map_str = '\n'.join([
			''.join([
				str(field) for field in row  # for every field
			]) for row in self.map  # in every row
		])
		return map_str

	def _initialize_field(self, x_pos, y_pos):
		"""Initializes and returns a single game field.

		:param x_pos: int, the field's position on the game map's x-axis
		:param y_pos: int, the field's position on the game map's y-axis
		:return: Field
		"""
		return Field(game_map=self, x_pos=x_pos, y_pos=y_pos)

	def __initialize_mines(self, start_x, start_y):
		"""Place the mines randomly on the map excluding the specified field - the player's first move.

		:param start_x: int, first-uncovered field's position on the x-axis
		:param start_y: int, first-uncovered field's position on the y-axis
		:return:
		"""

		# Get all possible field positions on the map.
		map_positions = {*itertools.product(range(self.width), range(self.height))}

		# Exclude the position specified by parameters.
		start_field = (start_x, start_y)
		map_positions.discard(start_field)

		# Randomly pick mine placement.
		map_positions_sample = random.sample(map_positions, self.mine_count)

		# Plant mines in selected positions.
		for x, y in map_positions_sample:
			self.map[y][x].set_mine(True)

		# Tag mines as initialized.
		self.mines_initialized = True

	def _initialize_map(self):
		""" Initializes a matrix of fields with set width and height.

		:return: Field matrix
		"""

		# Initialize all the fields.
		game_map = [[self._initialize_field(x_pos=x, y_pos=y) for x in range(self.width)] for y in range(self.height)]
		self.map = game_map

		# Tag mines as uninitialized.
		self.mines_initialized = False

		# Reset uncovered fields counter.
		self._uncovered = 0

	def get_map(self):
		"""Getter for map"""
		return self.map

	def get_field(self, x, y):
		"""Gets a Field at the specified position.

		:param x: int, the field's position on the game map's x-axis
		:param y: int, the field's position on the game map's y-axis
		:return: Field
		"""
		return self.map[y][x]

	def get_uncovered(self):
		"""Getter for the number of uncovered fields."""
		return self._uncovered

	def toggle_flag(self, x, y):
		"""Toggles the flag for the specified field.

		:param x: int, the field's position on the x-axis
		:param y: int, the field's position on the y-axis
		:return:
		"""

		field = self.map[y][x]

		# If field is already flagged or there are flags left to use.
		if field.is_flagged() or self.get_flags() < self.mine_count:

			# Toggle flag.
			field.toggle_flag()

			# Call counter function if assigned.
			if self.fun_count is not None:
				self.fun_count(self.mine_count-self.get_flags())

	def count_neighbors(self, x, y):
		""" Counts all the mines on the fields neighboring with the specified field.
		This includes the field at (x, y), which has no real effect on the game as uncovering a field with a mine
		results in a game over.

		:param x: int, the field's position on the game map's x-axis
		:param y: int, the field's position on the game map's y-axis
		:return: int, number of neighbors
		"""

		# Get indices of neighbors.
		x_indices = range(max(x-1, 0), min(x+2, self.width))
		y_indices = range(max(y-1, 0), min(y+2, self.height))

		# Count mines.
		mine_nbs = sum([
			self.map[y_check][x_check].has_mine()
			for x_check, y_check in itertools.product(x_indices, y_indices)
		])

		return mine_nbs

	def uncover(self, x, y):
		"""Uncovers the field at the specified position.

		:param x: int, the field's position on the game map's x-axis
		:param y: int, the field's position on the game map's y-axis
		:return:
		"""

		# Initialize mines on first move.
		if not self.mines_initialized:
			self.__initialize_mines(x, y)

		# Get specified field.
		field = self.map[y][x]

		# Only uncover if wasn't previously uncovered and is not flagged.
		if not field.is_visible() and not field.is_flagged():
			mine = field.has_mine()

			# Hit a mine.
			if mine:
				# Game Over
				self.fun_loss()
			else:
				# Uncover the field.
				field.uncover()

				# Count neighbors to show on uncovered field.
				mine_nbs = self.count_neighbors(x, y)
				field.set_neighbors(mine_nbs)

				# Also uncover neighbors if there are no mines around
				if not mine and mine_nbs == 0:
					x_indices = range(max(x - 1, 0), min(x + 2, self.width))
					y_indices = range(max(y - 1, 0), min(y + 2, self.height))
					for x_check, y_check in itertools.product(x_indices, y_indices):
						self.uncover(x_check, y_check)

				# Count all uncovered fields for easy win condition check.
				self._uncovered += 1

				if self.check_win_condition() and self.fun_win is not None:
					# Good Jowb
					self.fun_win()

	def check_win_condition(self):
		"""Checks the win condition for the game - are all the empty fields uncovered?

		:return: bool, whether the win condition was met
		"""
		return self._uncovered == self.width * self.height - self.mine_count

	def get_mine_count(self):
		"""Getter for the number of mines."""
		return self.mine_count

	def get_flags(self):
		"""Counts and returns the number of flags.

		:return: int
		"""

		flag_count = sum([field.is_flagged() for row in self.map for field in row])
		return flag_count

	def subscribe(self, win_func=None, loss_func=None, count_func=None):
		"""Assign functions to be called during game end or change in flag count.

		:param win_func: function to be called after all empty fields were uncovered
		:param loss_func: function to be called after a field containing a mine was uncovered
		:param count_func: function called after every change in the number of remaining flags
		:return:
		"""

		self.fun_win = win_func if win_func is not None else self.fun_win
		self.fun_loss = loss_func if loss_func is not None else self.fun_loss
		self.fun_count = count_func if count_func is not None else self.fun_count


class Field:
	def __init__(self, game_map, x_pos, y_pos):
		"""Representation of a single field on a game map.

		:param game_map: GameMap, the parent game map containing this field
		:param x_pos: int, this field's position on the game map's x-axis
		:param y_pos: int, this field's position on the game map's y-axis
		"""

		self.game_map = game_map
		self._mine = False
		self._visible = False
		self._mine_nbs = None
		self._flagged = False
		self.x = x_pos
		self.y = y_pos

	def __str__(self):
		return str(self._mine_nbs) if self._visible else 'X'

	def uncover(self):
		"""Uncovers this field."""

		self._visible = True

		# Flag must be explicitly set as false in case the field was uncovered due to a neighboring "0"
		self._flagged = False

	def get_position(self):
		"""Returns the position of this field on the game map.

		:return: (int, int)
		"""
		return self.x, self.y

	def is_visible(self):
		"""Getter for visible"""
		return self._visible

	def is_flagged(self):
		"""Getter for flagged."""
		return self._flagged

	def set_neighbors(self, nbs):
		"""Sets the number of neighboring mines for display.

		:param nbs: int, number of neighboring mines
		:return:
		"""
		self._mine_nbs = nbs

	def set_mine(self, mine):
		"""Setter for mine."""
		self._mine = mine

	def has_mine(self):
		"""Getter for mine."""
		return self._mine

	def toggle_flag(self):
		"""Toggles flag for this field."""
		self._flagged = not self._flagged

	def get_nbs(self):
		"""Returns the number of neighboring mines."""
		return self._mine_nbs

