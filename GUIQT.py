from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, QTimer, QTime, QUrl
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer, QSound
from minesweeper import *
from functools import partial
import random


SFX_DIG = [
	QSound(r"F:\User\Python Projects\saper\sound\dig1.wav"),
	QSound(r"F:\User\Python Projects\saper\sound\dig2.wav")
]


class FieldWidget(Field, QPushButton):
	def __init__(self, game_map, x_pos, y_pos):
		"""Button corresponding to a single field of a GameMap.

		:param game_map: GameMap, parent game map containing the field
		:param x_pos: int, position of the field on the game map's x axis
		:param y_pos: int, position of the field on the game map's y axis
		"""

		super().__init__(game_map=game_map, x_pos=x_pos, y_pos=y_pos)
		QPushButton.__init__(self)

		# Setup the look of the button.
		self.setStyleSheet("QPushButton {background-color: #28FF28; color: black;}")
		self.setFixedWidth(25)
		self.setFixedHeight(25)

		# Assign random digging sfx.
		self.sound = SFX_DIG[random.randint(0, 1)]

	def mousePressEvent(self, mouse_event):
		"""Mouse press event handler. Handles both left and right clicks.

		:param mouse_event: event corresponding to the mouse press
		:return:
		"""

		# Left mouse click.
		if mouse_event.button() == Qt.LeftButton:
			# Uncover the field and play sfx.
			self.game_map.uncover(*self.get_position())
			self.sound.play()
		# Right mouse click.
		elif mouse_event.button() == Qt.RightButton:
			# Flag the field and change the color.
			self.game_map.toggle_flag(self.x, self.y)
			if self.is_flagged():
				self.setStyleSheet("QPushButton {background-color: #FFFF88; color: black;}")
			else:
				self.setStyleSheet("QPushButton {background-color: #28FF28; color: black;}")

	def set_neighbors(self, nbs):
		"""Field method override. Disables the button, sets the label and changes color.

		:param nbs: int, he number of neighboring mines.
		:return:
		"""

		super().set_neighbors(nbs)
		self.setDisabled(True)
		self.setText(str(self.get_nbs()))
		self.setStyleSheet("QPushButton {background-color: #EEEEEE; color: black;}")


class GameMapWidget(Game, QWidget):
	def __init__(self, loss_func, win_func, count_func):
		"""Widget corresponding to the GameMap.

		:param loss_func: function called after a mine is hit
		:param win_func: function called after all fields ar uncovered
		:param count_func: function called after any field is flagged with the remaining number of flags
		"""

		super().__init__(**DIFF_EASY)
		QWidget.__init__(self)

		# Assign passed functions.
		self.subscribe(loss_func=loss_func, win_func=win_func, count_func=count_func)

		# Grid layout setup.
		self.grid_layout = QGridLayout(self)
		self.grid_layout.setContentsMargins(0, 0, 0, 0)
		self.grid_layout.setSpacing(0)

		# Add all field widgets to layout.
		for row in self.map:
			for field in row:
				self.grid_layout.addWidget(field, *reversed(field.get_position()))

	def _initialize_field(self, x_pos, y_pos):
		"""GameMap method override. Initializes and returns a FieldWidget.

		:param x_pos: int, position of the field on the x axis
		:param y_pos: int, position of the field on the y axis
		:return: FieldWidget
		"""

		return FieldWidget(game_map=self, x_pos=x_pos, y_pos=y_pos)

	def start_game(self, width, height, mine_count):
		"""Starts a new game with given parameters.

		:param width: int, width of the game map
		:param height: int, height of the game map
		:param mine_count: int, number of mines hidden on the map
		:return:
		"""

		# Change game parameters.
		self.width = width
		self.height = height
		self.mine_count = mine_count

		# Remove old widgets.
		for i in reversed(range(self.grid_layout.count())):
			self.grid_layout.itemAt(i).widget().setParent(None)

		# Initialize new map.
		self._initialize_map()
		for row in self.map:
			for field in row:
				self.grid_layout.addWidget(field, *reversed(field.get_position()))


class CustomGameSettings(QWidget):
	def __init__(self, game_window):
		"""Custom game settings pop-up window widget.

		:param game_window: GameWindow, parent window.
		"""

		super().__init__()
		self.__game_window = game_window

		self.setWindowTitle("Gra niestandardowa")

		# Form layout.
		layout = QFormLayout()
		self.setLayout(layout)

		# Width spin box.
		self.__input_width = self.__init_input(MIN_WIDTH, MAX_WIDTH)
		layout.addRow("Szerokość", self.__input_width)

		# Height spin box.
		self.__input_height = self.__init_input(MIN_HEIGHT, MAX_HEIGHT)
		layout.addRow("Wysokość", self.__input_height)

		# Mines spin box.
		self.__input_mines = self.__init_input(1, MIN_WIDTH * MIN_HEIGHT - 1)
		layout.addRow("Miny", self.__input_mines)

		# Confirm button.
		self.__btn_confirm = QPushButton("OK")
		self.__btn_confirm.clicked.connect(self.confirm_custom)
		layout.addWidget(self.__btn_confirm)

	def __init_input(self, min_val, max_val):
		"""Initializes a spin box with a set minimum and maximum value.

		:param min_val: int, minimum value
		:param max_val: int, maximum value
		:return: QSpinBox
		"""

		spin_box = QSpinBox()
		spin_box.setMinimum(min_val)
		spin_box.setMaximum(max_val)
		spin_box.setSingleStep(1)
		spin_box.valueChanged.connect(self.update_limits)
		return spin_box

	def update_limits(self):
		"""Updates the maximum value for the mine count spin box.
		The number of mines can't exceed the total number of fields on the map minus 1.
		"""

		self.__input_mines.setMaximum(self.__input_height.value() * self.__input_width.value() - 1)

	def confirm_custom(self):
		"""Starts a new game with selected parameters and hides the settings window."""

		self.__game_window.start_game(
			bname="custom",
			width=self.__input_width.value(),
			height=self.__input_height.value(),
			mine_count=self.__input_mines.value())
		self.hide()


class LeaderBoardWidget(QWidget):
	def __init__(self, leader_board):
		"""Leader board window widget.

		:param leader_board: LeaderBoard containing the scores to be displayed
		"""

		super().__init__()
		self.leader_board = leader_board

		# Window title.
		self.setWindowTitle("Tablica wyników")

		# Leader board widget setup for every difficulty.
		self.lb_easy = self.__init_lb("łatwy")
		self.lb_medium = self.__init_lb("średni")
		self.lb_hard = self.__init_lb("trudny")

		# Layout setup.
		lb_layout = QGridLayout()
		lb_layout.addWidget(QLabel("<b>Łatwy</b>"), 0, 0)
		lb_layout.addWidget(self.lb_easy, 1, 0)
		lb_layout.addWidget(QLabel("<b>Średni</b>"), 0, 1)
		lb_layout.addWidget(self.lb_medium, 1, 1)
		lb_layout.addWidget(QLabel("<b>Trudny</b>"), 0, 2)
		lb_layout.addWidget(self.lb_hard, 1, 2)
		self.setLayout(lb_layout)

	def __init_lb(self, lb_name):
		"""Initializes and returns a single leader board widget.

		:param lb_name: str, name of the leader board
		:return: QFrame displaying the leader board
		"""

		# QFrame widget setup.
		lb_widget = QFrame()
		lb_widget.setFrameStyle(QFrame.Panel)

		# Layout setup.
		lb_layout = QGridLayout()
		lb_widget.setLayout(lb_layout)

		# Leader board scores setup.
		self.__refresh_scores(lb_layout, lb_name)

		return lb_widget

	def __refresh_scores(self, lb_layout, lb_name):
		"""Refreshes the scores from a single leader-board. Used internally.

		:param lb_layout: layout widget corresponding to the leader board
		:param lb_name: str, the name of the leader board
		:return:
		"""

		# Remove old widgets.
		for i in reversed(range(lb_layout.count())):
			lb_layout.itemAt(i).widget().setParent(None)

		# Add scores.
		for i, score in enumerate(self.leader_board.get_scores(lb_name)):
			for j, lab in enumerate(f"{i+1}. {score.get_name()} {score.get_game_time()} {score.get_date()}".split(" ")):
				lb_layout.addWidget(QLabel(lab), i, j)

		# Fill with placeholders up to 10 places.
		for i in range(len(self.leader_board.get_scores(lb_name)), 10):
			for j, lab in enumerate(f"{i+1}. - - -".split(" ")):
				lb_layout.addWidget(QLabel(lab), i, j)

	def refresh_scores(self):
		"""Refreshes the scores for all leader boards."""

		for lb_name, lb_widget in zip(["łatwy", "średni", "trudny"], [self.lb_easy, self.lb_medium, self.lb_hard]):
			lb_layout = lb_widget.layout()
			self.__refresh_scores(lb_layout, lb_name)


class GameWindow(QMainWindow):
	def __init__(self):
		"""Main window of the application."""
		super().__init__()

		# Banger soundtrack.
		url = QUrl.fromLocalFile("./sound/Getting it Done.mp3")
		content = QMediaContent(url)
		self.player = QMediaPlayer()
		self.player.setMedia(content)
		self.player.setVolume(20)
		self.player.play()

		# Window title.
		self.setWindowTitle("Saper")

		# Leader board setup.
		self.leader_board = LeaderBoard("leaderboard.json", ["łatwy", "średni", "trudny"])

		# GameMapWidget setup, initializes the game on easy difficulty.
		self.game_widget = GameMapWidget(win_func=self.win_game, loss_func=self.game_over, count_func=self.update_mine_counter)
		self.bname = "łatwy"

		# Game timer setup.
		self.timer = QTimer()
		self.time = QTime(0, 0, 0)
		self.timer.timeout.connect(self.timer_event)
		self.timer.start(1000)
		self.timer_label = QLabel()

		# Custom game map settings window.
		self.custom_settings = CustomGameSettings(self)

		# Menu bar setup.
		menu_bar = self.menuBar()

		# About dialog box
		self.about_dialog = QMessageBox()
		self.about_dialog.setIcon(QMessageBox.Information)
		self.about_dialog.setText("""
			<b>Aplikacja</b><br/>
			Tomasz Wierciński<br/>
			Stworzona w ramach przedmiotu "Języki skryptowe i ich zastosowania"<br/>
			Politechnika Gdańska<br/><br/>
			<b>Muzyka</b><br/>
			"Getting it Done" Kevin MacLeod (incompetech.com)<br/>
			Licensed under Creative Commons: <a href="http://creativecommons.org/licenses/by/4.0/">
			By Attribution 4.0 License</a><br/><br/>
			Efekty dźwiękowe: zapsplat.com
		""".strip().replace('\t', ''))
		self.about_dialog.setWindowTitle("O aplikacji")

		# Help sub-menu
		help_menu = QMenu("&Pomoc", self)
		help_menu.addAction("O &aplikacji").triggered.connect(self.about_dialog.exec_)
		menu_bar.addMenu(help_menu)

		# New game sub-menu.
		new_game_menu = QMenu("&Nowa gra", self)
		new_game_menu.addAction("Łatwy")\
			.triggered.connect(partial(self.start_game, bname="łatwy", **DIFF_EASY))
		new_game_menu.addAction("Średni")\
			.triggered.connect(partial(self.start_game, bname="średni", **DIFF_MEDIUM))
		new_game_menu.addAction("Trudny")\
			.triggered.connect(partial(self.start_game, bname="trudny", **DIFF_HARD))
		new_game_menu.addAction("Niestandardowy").triggered.connect(self.open_custom_settings)
		menu_bar.addMenu(new_game_menu)

		# Leaderboard action button.
		self.leader_board_widget = LeaderBoardWidget(self.leader_board)
		menu_bar.addAction("&Tablica wyników").triggered.connect(self.leader_board_widget.show)

		# Counter setup
		self.counter_label = QLabel(str(self.game_widget.get_mine_count()))

		# Central widget with layout setup.
		self.sub_layout = QGridLayout()
		self.sub_layout.setSpacing(10)
		self.sub_layout.addWidget(self.game_widget, 1, 0, 1, 2)
		self.sub_layout.addWidget(self.timer_label, 0, 0)
		self.sub_layout.addWidget(self.counter_label, 0, 1)
		sub_widget = QWidget()
		sub_widget.setLayout(self.sub_layout)
		self.setCentralWidget(sub_widget)
		self.resize_window()

	def timer_event(self):
		"""Timer event triggered every second."""

		# Adds one second to the timer and updates the timer label widget.
		self.time = self.time.addSecs(1)
		self.timer_label.setText(self.time.toString("hh:mm:ss"))

	def update_mine_counter(self, count):
		"""Changes the value of the mine counter label. Called every time a flag is placed or removed.

		:param count: int, number of remaining flags.
		:return:
		"""

		self.counter_label.setText(str(count))

	def start_game(self, bname, width, height, mine_count):
		"""Starts a new game with specified parameters.

		:param bname: str, leader board name (difficulty)
		:param width: int, width of the game map
		:param height: int, height of the game map
		:param mine_count: int, number of mines hidden on the game map
		:return:
		"""

		self.bname = bname
		self.game_widget.start_game(width=width, height=height, mine_count=mine_count)
		self.resize_window()
		self.counter_label.setText(str(self.game_widget.get_mine_count()))

		# Reset timer.
		self.time = QTime(0, 0, 0)
		self.timer.start(1000)

	def resize_window(self):
		"""Resize the window based on the game map size."""

		# Set the left and right margins.
		ew_margins = max(10, int((300 - self.game_widget.width * 25) / 2))
		self.sub_layout.setContentsMargins(ew_margins, 10, ew_margins, 10)

		# Gotta make sure all the events are processed otherwise the minimumSizeHint will be wrong.
		for i in range(0, 10):
			QApplication.processEvents()

		# Resize to minimum possible size.
		self.setFixedSize(self.minimumSizeHint())

	def open_custom_settings(self):
		"""Shows the custom game settings window."""
		self.custom_settings.show()

	def game_over(self):
		"""Stops the game and changes the game color scheme. Called when a mine is hit."""

		# Stop the timer.
		self.timer.stop()

		# Change the color scheme and disable all buttons.
		game_map = self.game_widget.get_map()
		for row in game_map:
			for field in row:
				field.setDisabled(True)
				if field.has_mine():
					field.setStyleSheet("QPushButton {background-color: #FF4444; color: white;}")
				elif field.is_visible():
					field.setText(str(field.get_nbs()))
					field.setStyleSheet("QPushButton {background-color: #EEEEEE; color: black;}")
				else:
					field.setStyleSheet("QPushButton {background-color: #AAAAAA; color: black;}")

	def win_game(self):
		"""Stops the game and asks for the player's name if the score can be added to the leader board."""

		# Stop the timer.
		self.timer.stop()

		# If game is not custom and score can be added.
		if self.bname != "custom" and self.leader_board.check_score(self.bname, self.time.second()):
			# Ask for player's name.
			text, ok = QInputDialog.getText(self, "Wygrana", f"Wpisz się do tablicy wyników!\nCzas: {self.time.second()}s")
			if ok:
				# Add score to leader board and refresh leader board widget.
				self.leader_board.add_score(self.bname, self.time.second(), str(text))
				self.leader_board.write_scores()
				self.leader_board_widget.refresh_scores()


if __name__ == "__main__":
	app = QApplication([])
	app.setStyle("Fusion")

	game_window = GameWindow()
	game_window.show()

	app.exec_()
