from minesweeper import *
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib
from functools import partial


class FieldWidget(Field, Gtk.Button):
	def __init__(self, game_map, x_pos, y_pos):
		"""Button corresponding to a single field of a GameMap.

		:param game_map: GameMap, parent game map containing the field
		:param x_pos: int, position of the field on the game map's x axis
		:param y_pos: int, position of the field on the game map's y axis
		"""

		super().__init__(game_map=game_map, x_pos=x_pos, y_pos=y_pos)
		Gtk.Button.__init__(self)
		self.set_size_request(25, 25)
		self.connect("event", self.mouse_press_event)

	def set_neighbors(self, nbs):
		"""Field method override. Disables the button, sets the label and adds "uncovered" class.

		:param nbs: int, he number of neighboring mines.
		:return:
		"""

		super().set_neighbors(nbs)
		self.set_sensitive(False)
		self.get_style_context().add_class("uncovered")

		# Button label setup.
		label = Gtk.Label(label=str(self.get_nbs()))
		label.set_padding(0, 0)
		self.add(label)
		label.show()

	def mouse_press_event(self, _, event):
		"""Mouse press event handler. Handles both left and right clicks.

		:param event: event corresponding to the mouse press
		:return:
		"""

		if event.type == Gdk.EventType.BUTTON_PRESS:
			# Right click.
			if event.get_button().button == 3:
				# Toggle the flag and "flagged" class.
				self.game_map.toggle_flag(self.x, self.y)
				if self.is_flagged():
					self.get_style_context().add_class("flagged")
				else:
					self.get_style_context().remove_class("flagged")
			# Left click
			else:
				# Uncover the field.
				self.game_map.uncover(self.x, self.y)
			return True

	def set_mine(self, mine):
		"""Field method override. Adds the "mine" class to button.

		:param mine: bool, whether the field contains a mine
		:return:
		"""

		super().set_mine(mine=mine)
		if mine:
			self.get_style_context().add_class("mine")


class GameMapWidget(Game, Gtk.Grid):
	def __init__(self, loss_func, win_func, count_func):
		"""Widget corresponding to the GameMap.

		:param loss_func: function called after a mine is hit
		:param win_func: function called after all fields ar uncovered
		:param count_func: function called after any field is flagged with the remaining number of flags
		"""

		super().__init__(**DIFF_EASY)
		Gtk.Grid.__init__(self)

		# Assign passed functions.
		self.subscribe(loss_func=loss_func, win_func=win_func, count_func=count_func)

		# Add all field widgets to layout.
		for row in self.map:
			for field in row:
				self.attach(field, *field.get_position(), 1, 1)

	def _initialize_field(self, x_pos, y_pos):
		"""GameMap method override. Initializes and returns a FieldWidget.

		:param x_pos: int, position of the field on the x axis
		:param y_pos: int, position of the field on the y axis
		:return: FieldWidget
		"""

		return FieldWidget(game_map=self, x_pos=x_pos, y_pos=y_pos)

	def start_game(self, width, height, mine_count):
		"""Starts a new game with given parameters.

		:param width: int, width of the game map
		:param height: int, height of the game map
		:param mine_count: int, number of mines hidden on the map
		:return:
		"""

		# Change game parameters
		self.width = width
		self.height = height
		self.mine_count = mine_count

		# Remove old widgets.
		for row in self.map:
			for field in row:
				self.remove(field)

		# Initialize new map.
		self._initialize_map()
		for row in self.map:
			for field in row:
				self.attach(field, *field.get_position(), 1, 1)
				field.show()


class CustomGameWindow(Gtk.Window):
	def __init__(self, game_window, width, height, mine_count):
		"""Custom game settings pop-up window.

		:param game_window: GameWindow, parent window.
		"""

		super().__init__(title="Gra niestandardowa")
		self.__game_window = game_window

		# Grid layout.
		self.__layout = Gtk.ListBox()
		self.add(self.__layout)

		# Input spin boxes.
		self.__input_width = self.__init_list_row("Szerokość", MIN_WIDTH, MAX_WIDTH, width)
		self.__input_height = self.__init_list_row("Wysokość", MIN_HEIGHT, MAX_HEIGHT, height)
		self.__input_mines = self.__init_list_row("Miny", 1, width*height-1, mine_count)

		# Confirm button.
		self.__btn_confirm = Gtk.Button(label="Zatwierdź")
		self.__btn_confirm.connect("clicked", self.confirm_custom)
		btn_confirm_row = Gtk.ListBoxRow()
		btn_confirm_row.add(self.__btn_confirm)
		self.__layout.add(btn_confirm_row)

	def __init_list_row(self, label, min_val, max_val, val):
		"""Initializes a list row box with a spin box and a label.

		:param label: str, spin box label
		:param min_val: int, minimum value
		:param max_val: int, maximum value
		:param val: int, current value
		:return: QSpinBox
		"""

		row = Gtk.ListBoxRow()

		# Spin box setup.
		spin_box = Gtk.SpinButton(
			adjustment=Gtk.Adjustment(
				value=min_val,
				lower=min_val,
				upper=max_val,
				step_increment=1,
				page_increment=0,
				page_size=0),
			climb_rate=1., digits=0)
		spin_box.set_value(val)
		spin_box.connect("input", self.update_limits)

		# Horizontal layout setup.
		hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
		hbox.pack_start(Gtk.Label(label=label), True, True, 0)
		hbox.pack_end(spin_box, True, True, 0)

		row.add(hbox)
		self.__layout.add(row)
		return spin_box

	def update_limits(self, spin_button, value):
		"""Updates the maximum value for the mine count spin box.
		The number of mines can't exceed the total number of fields on the map minus 1.

		:param spin_button: SpinButton, the spin button that triggered the event. Unused.
		:param value: the value of the spin button that triggered the event. Unused.
		"""

		max_mines = self.__input_width.get_value_as_int() * self.__input_height.get_value_as_int() - 1
		self.__input_mines.set_range(1, max_mines)

	def confirm_custom(self, event):
		"""Starts a new game with selected parameters and destroys the settings window."""
		self.__game_window.start_game(
			event,
			bname="custom",
			width=self.__input_width.get_value_as_int(),
			height=self.__input_height.get_value_as_int(),
			mine_count=self.__input_mines.get_value_as_int()
		)
		self.destroy()


class LeaderBoardWindow(Gtk.Window):
	def __init__(self, leader_board):
		"""Leader board window widget.

		:param leader_board: LeaderBoard containing the scores to be displayed
		"""

		super().__init__(type=Gtk.WindowType.TOPLEVEL, title="Tablica wyników")
		self.leader_board = leader_board

		# Leader board widget setup for every difficulty.
		self.lb_easy = self.__init_lb("łatwy")
		self.lb_medium = self.__init_lb("średni")
		self.lb_hard = self.__init_lb("trudny")

		# Layout setup.
		lb_layout = Gtk.Grid()

		# Layout setup - easy difficulty leader board.
		lb_layout.attach(Gtk.Label(label="Łatwy"), 0, 0, 1, 1)
		lb_layout.attach(Gtk.HSeparator(), 0, 1, 1, 1)
		lb_layout.attach(self.lb_easy, 0, 2, 1, 1)

		lb_layout.attach(Gtk.VSeparator(), 1, 2, 1, 1)

		# Layout setup - medium difficulty leader board.
		lb_layout.attach(Gtk.Label(label="Średni"), 2, 0, 1, 1)
		lb_layout.attach(Gtk.HSeparator(), 2, 1, 1, 1)
		lb_layout.attach(self.lb_medium, 2, 2, 1, 1)

		lb_layout.attach(Gtk.VSeparator(), 3, 2, 1, 1)

		# Layout setup - hard difficulty leader board.
		lb_layout.attach(Gtk.Label(label="Trudny"), 4, 0, 1, 1)
		lb_layout.attach(Gtk.HSeparator(), 4, 1, 1, 1)
		lb_layout.attach(self.lb_hard, 4, 2, 1, 1)

		self.add(lb_layout)

	def __init_lb(self, lb_name):
		"""Initializes and returns a single leader board widget.

		:param lb_name: str, name of the leader board
		:return: QFrame displaying the leader board
		"""

		lb_grid = Gtk.Grid()
		self.__refresh_scores(lb_grid, lb_name)
		lb_grid.set_halign(Gtk.Align.START)
		return lb_grid

	def __refresh_scores(self, lb_layout, lb_name):
		"""Refreshes the scores from a single leader-board. Used internally.

		:param lb_layout: layout widget corresponding to the leader board
		:param lb_name: str, the name of the leader board
		:return:
		"""

		# Remove old widgets.
		for i in range(10):
			lb_layout.remove_row(i)

		# Get scores.
		score_list = self.leader_board.get_scores(lb_name)

		# Prepare grid row values.
		row_values = [
			(f"{i+1}. {score.get_name()}", f"{score.get_game_time()}", f"{score.get_date()}")
			for i, score in enumerate(score_list)]
		row_values += [
			(f"{i+1}. -", "-", "-")
			for i in range(len(row_values), 10)]

		# Add score widgets.
		for row_i, row in enumerate(row_values):
			for col_i, val in enumerate(row):
				lab = Gtk.Label(label=val)
				lab.set_padding(10, 0)
				lab.set_justify(Gtk.Justification.LEFT)
				lb_layout.attach(lab, col_i * 2, row_i, 1, 1)
			lb_layout.attach(Gtk.VSeparator(), 1, row_i, 1, 1)
			lb_layout.attach(Gtk.VSeparator(), 3, row_i, 1, 1)


class GameWindow(Gtk.Window):
	def __init__(self):
		"""Main window of the application."""
		super().__init__(title="Saper")

		# Leader board setup.
		self.leader_board = LeaderBoard("leaderboard.json", ["łatwy", "średni", "trudny"])

		# GameMapWidget setup, initializes the game on easy difficulty.
		self.game_widget = GameMapWidget(win_func=self.win_game, loss_func=self.game_over, count_func=self.update_mine_counter)
		self.bname = "łatwy"

		# Game timer setup.
		self.timer = GLib.timeout_add(1000, self.timer_event)
		self.time = 0
		self.timer_label = Gtk.Label()
		self.timer_is_running = True

		# Menu bar setup.
		mb = Gtk.MenuBar()

		# Help sub-menu.
		help_menu_item = Gtk.MenuItem(label="Pomoc")
		help_sub_menu = Gtk.Menu()
		about_menu_item = Gtk.MenuItem(label="O aplikacji")
		about_menu_item.connect("activate", self.show_app_info)
		help_sub_menu.append(about_menu_item)
		help_menu_item.set_submenu(help_sub_menu)
		mb.append(help_menu_item)

		# Leader board button.
		lb_menu_item = Gtk.MenuItem(label="Tablica wyników")
		lb_menu_item.connect("activate", self.show_leader_board)
		mb.append(lb_menu_item)

		# New game sub-menu
		new_game_menu = Gtk.Menu()
		new_game_menu_item = Gtk.MenuItem(label="Nowa gra")
		new_game_menu_item.set_submenu(new_game_menu)
		mb.append(new_game_menu_item)

		# New game sub-menu - easy
		easy_menu_item = Gtk.MenuItem(label="Łatwy")
		easy_menu_item.connect(
			"activate", partial(self.start_game, bname="łatwy", **DIFF_EASY))
		new_game_menu.append(easy_menu_item)

		# New game sub-menu - medium
		medium_menu_item = Gtk.MenuItem(label="Średni")
		medium_menu_item.connect(
			"activate", partial(self.start_game, bname="średni", **DIFF_MEDIUM))
		new_game_menu.append(medium_menu_item)

		# New game sub-menu - hard
		hard_menu_item = Gtk.MenuItem(label="Trudny")
		hard_menu_item.connect(
			"activate", partial(self.start_game, bname="trudny", **DIFF_HARD))
		new_game_menu.append(hard_menu_item)

		# New game sub-menu - custom
		custom_menu_item = Gtk.MenuItem(label="Niestandardowy")
		custom_menu_item.connect("activate", self.show_custom_settings)
		new_game_menu.append(custom_menu_item)

		# Counter setup
		self.counter_label = Gtk.Label(str(self.game_widget.get_mine_count()))

		# Layout setup
		hbox = Gtk.HBox(homogeneous=True, spacing=10)
		hbox.pack_start(self.timer_label, False, False, 0)
		hbox.pack_end(self.counter_label, False, False, 0)
		self.vbox = Gtk.VBox(homogeneous=False, spacing=2)
		self.vbox.pack_start(mb, False, False, 0)
		self.vbox.pack_start(hbox, False, False, 0)
		self.vbox.pack_end(self.game_widget, False, False, 0)
		self.add(self.vbox)

	def update_mine_counter(self, count):
		"""Changes the value of the mine counter label. Called every time a flag is placed or removed.

		:param count: int, number of remaining flags.
		:return:
		"""
		self.counter_label.set_label(str(count))

	def timer_event(self):
		"""Timer event triggered every second."""

		if self.timer_is_running:
			# Add one second.
			self.time += 1

			# Convert seconds to minutes and hours.
			m, s = divmod(self.time, 60)
			h, m = divmod(m, 60)

			# Show the time.
			self.timer_label.set_label(f"{h}:{m:02}:{s:02}")
		return True

	def show_leader_board(self, _):
		"""Shows the leader board window."""

		leader_board_window = LeaderBoardWindow(self.leader_board)
		leader_board_window.show_all()

	def show_custom_settings(self, _):
		"""Shows the custom game settings window."""

		custom_game_window = CustomGameWindow(
			game_window=self,
			width=self.game_widget.width,
			height=self.game_widget.height,
			mine_count=self.game_widget.get_mine_count()
		)
		custom_game_window.show_all()

	def win_game(self):
		"""Stops the game and asks for the player's name if the score can be added to the leader board."""

		# Stop the timer.
		self.timer_is_running = False

		# If game is not custom and score can be added.
		if self.bname != "custom" and self.leader_board.check_score(self.bname, self.time):
			# Ask for player's name.
			response = self.get_name()
			if len(response) > 0:
				# Add score to leader board.
				self.leader_board.add_score(self.bname, self.time, str(response))
				self.leader_board.write_scores()

	def get_name(self):
		"""Ask the player for their name using a MessageDialog.

		:return: str, entered name
		"""

		# Message dialog setup.
		dialog = Gtk.MessageDialog(
			transient_for=self,
			flags=0,
			message_type=Gtk.MessageType.QUESTION,
			buttons=Gtk.ButtonsType.OK,
			text="Wygrana")
		dialog.format_secondary_markup(
			f'Wpisz się do <u>tablicy wyników</u>!\nCzas: {self.time}s')

		# Text input field.
		entry = Gtk.Entry()

		# Horizontal layout for the label-input pair.
		hbox = Gtk.HBox()
		hbox.pack_start(Gtk.Label("Name:"), False, 5, 5)
		hbox.pack_end(entry, True, True, 0)
		dialog.vbox.pack_end(hbox, True, True, 0)

		# Show the dialog and get response.
		dialog.show_all()
		dialog.run()
		text = entry.get_text()

		dialog.destroy()
		return text

	def show_app_info(self, _):
		"""Shows the app info using a MessageDialog."""

		dialog = Gtk.MessageDialog(
			transient_for=self,
			flags=0,
			message_type=Gtk.MessageType.INFO,
			buttons=Gtk.ButtonsType.OK,
			text="O aplikacji",
		)

		# Uses Pango markup language.
		dialog.format_secondary_markup("""
			<b>Aplikacja</b>
			Tomasz Wierciński
			Stworzona w ramach przedmiotu "Języki skryptowe i ich zastosowania"
			Politechnika Gdańska
		""".strip().replace('\t', ''))
		dialog.run()
		dialog.destroy()

	def start_game(self, event, bname, width, height, mine_count):
		"""Starts a new game with specified parameters.

		:param event: event that triggered the method. Unused.
		:param bname: str, leader board name (difficulty)
		:param width: int, width of the game map
		:param height: int, height of the game map
		:param mine_count: int, number of mines hidden on the game map
		:return:
		"""

		Gtk.StyleContext.remove_class(self.game_widget.get_style_context(), "game-over")
		self.bname = bname
		self.game_widget.start_game(width=width, height=height, mine_count=mine_count)
		self.resize_window()

		# Reset timer.
		self.time = 0
		self.timer_is_running = True

	def resize_window(self):
		"""Resize the window to smallest possible size."""
		self.resize(1, 1)

	def game_over(self):
		"""Stops the game and changes the game color scheme. Called when a mine is hit."""

		# Stop the timer.
		self.timer_is_running = False

		# Change the color scheme - add "game-over" class.
		Gtk.StyleContext.add_class(self.game_widget.get_style_context(), "game-over")

		# Disable all field buttons.
		game_map = self.game_widget.get_map()
		for row in game_map:
			for field in row:
				field.set_sensitive(False)


if __name__ == "__main__":
	# Get style from css file.
	cssProvider = Gtk.CssProvider()
	cssProvider.load_from_path("GTKstyle.css")
	screen = Gdk.Screen.get_default()
	styleContext = Gtk.StyleContext()
	styleContext.add_provider_for_screen(
		screen, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_USER)

	# Initialize and show the main window.
	win = GameWindow()
	win.connect("destroy", Gtk.main_quit)
	win.show_all()
	Gtk.main()
